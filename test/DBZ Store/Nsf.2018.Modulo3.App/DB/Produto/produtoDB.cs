﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class produtoDB
    {
        public int Salvar(produtoDTO dto)
        {
            string script = @"insert into tb_produto
            (
            nm_produto
            vl_preco
            )
            VALUES
            (
            @nm_produto
            @vl_preco
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nome));
            parms.Add(new MySqlParameter("vl_preco", dto.preco));

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parms);
        }    
                public List<produtoDTO> Consultar(string produto, decimal preco)
        {
            string script =
                @"SELECT *
                      FROM tb_produto
                   WHERE nm_produto like @nm_produto
                    AND vl_preco like @vl_preco";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + produto + "%"));
            parms.Add(new MySqlParameter("vl_preco", "%" + preco + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<produtoDTO> produtos = new List<produtoDTO>();
            
            while (reader.Read())
            {
                produtoDTO novoProduto = new produtoDTO();
                novoProduto.id = reader.GetInt32("id_produto");
                novoProduto.nome = reader.GetString("nm_produto");
                novoProduto.preco = reader.GetDecimal("vl_preco");
                produtos.Add(novoProduto);
            }
            reader.Close();

            return produtos;
        }
    }
    
}
